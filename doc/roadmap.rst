Road map
--------

The roadmap has been moved to the project's gitlab site:

https://gitlab.com/thecker/gimp-msx-plugins/-/milestones


Please add requests for enhancements or bug fixes on gitlab as well:

https://gitlab.com/thecker/gimp-msx-plugins/issues