.. gimp-msx-plugins documentation master file, created by
   sphinx-quickstart on Thu Nov 14 20:46:49 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to gimp-msx-plugins's documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
	
   intro
   install_link
   user_guide/user_guide
   dev_guide/developer_guide
   changelog
   roadmap


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
