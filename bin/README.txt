gimp-msx-plugins
================

How to install
--------------

To install the plug-ins you just need to copy the files in the plug-in and 
palettes folders to the corresponding folders of your GIMP install.

You can check in GIMP where these folders are.

In GIMP go to:

Edit --> Preferences --> Folders --> Plug-ins

and/or

Edit --> Preferences --> Folders --> Palettes

For more information on using the software please visit the project's 
documentation homepage:

https://gimp-msx-plugins.readthedocs.io/en/latest/

You can obtain the source code from:

https://gitlab.com/thecker/gimp-msx-plugins

License note
------------

Copyright (C) 2019, 2020 Thies Hecker

gimp-msx-plugins is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

The MSX GMII exporter plug-in is based on the "GIMP Plug-in template", 
Copyright (C) 2000-2004 Michael Natterer
