/**
 * types.h - structs and enum types defintions for GIMP MSX plugins
 * Copyright (C) 2019 Thies Hecker
 *
 * This file is part of the gimp-msx-plugins.
 *
 * gimp-msx-plugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <libgimp/gimp.h>

#ifndef SRC_TYPES_H_
#define SRC_TYPES_H_

/// struct to store the plug-in parameters
typedef struct
{
  ///consider #ff00eb as transparent color
  guchar transp_setting;
  ///add a header to each file for BLOAD command
  gboolean add_bload;
  ///perform a check only - do not export
  gboolean check_only;
  ///compress size by identifying reused blocks
  gboolean compress;
  ///split files into screen regions
  gboolean split_files;
  ///export a ROM image
  gboolean export_asm;
  ///dither mode number
  gint dither_mode;
  ///indexer: 0 = internal, 1 = MSXize
  gint indexer;
  ///back drop color for ROM image
  gint bg_color;

} ExportValues;

/// struct type to store the image data for each of the 3 screen regions
typedef struct {
	/// name table
	guchar nt_data[3][256];
	/// color table
	guchar ct_data[3][2048];
	/// pattern generator table
	guchar pt_data[3][2048];
	/// number of unique blocks
	guint16 unique_blocks[3];	// max. could be 256 - i.e. 0x0100
} ImageData;


enum TransparencyOptions {USE_ALPHA, USE_COLOR_FF00EB, NO_TRANSPARENCY};

enum screen_regions {TOP, MID, BUTTOM};

///available indexer values
/**
 * gimp-image-covert-indexed or plug-in-msxize
 */
enum indexer {Internal, MSXize};

#endif /* SRC_TYPES_H_ */
