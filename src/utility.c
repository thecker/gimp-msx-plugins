/**
 * utility.c - utility functions for GIMP MSX plugins
 * Copyright (C) 2019 Thies Hecker
 *
 * This file is part of the gimp-msx-plugins.
 *
 * gimp-msx-plugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <libgimp/gimp.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <libgimp/gimpui.h>
#include "types.h"

gboolean check_option_combinations(ExportValues plugin_options){
	//TODO: implement option combination check ups...
	gboolean valid = TRUE;

	if (plugin_options.compress && !plugin_options.split_files){
		valid = FALSE;
		g_message("\"Compress image\" option can only be used if \"split files\" is activated, too.\n"
				"Please adjust the options accordingly.\n");
	}

	return valid;
}

void resize_image(gint32 image_id){
	//TODO: implement resize function...
	gint32 drawable_id;
	GimpDrawable *drawable;
	gint width, height, scaled_width, scaled_height;
	gdouble width_factor, height_factor;

	drawable_id = gimp_image_get_active_drawable(image_id);
	drawable = gimp_drawable_get(drawable_id);

	width = drawable->width;
	height = drawable->height;

	width_factor = (double) width / (double) 256;
	height_factor = (double) height / (double) 192;

	if (width < 256 && height < 192){	//do not scale up, but resize
		gimp_image_resize(image_id, 256, 192, 0, 0);
	}
	else if (width > 256 || height > 192){	//scale down, if any dimension too large
		if (width_factor >= height_factor){
			gimp_image_scale(image_id, 256, (gint) height/width_factor);
			drawable_id = gimp_image_get_active_drawable(image_id);
			drawable = gimp_drawable_get(drawable_id);
			scaled_height = drawable->height;
			if (scaled_height < 192){
				gimp_image_resize(image_id, 256, 192, 0, (192-scaled_height)/2);
			}
		}
		else{
			gimp_image_scale(image_id, (gint) width/height_factor, 192);
			drawable_id = gimp_image_get_active_drawable(image_id);
			drawable = gimp_drawable_get(drawable_id);
			scaled_width = drawable->width;
			if (scaled_width < 256){
				gimp_image_resize(image_id, 256, 192, (256-scaled_width)/2, 0);
			}
		}
	}
	if (gimp_drawable_is_layer(drawable_id)){
		gimp_layer_resize_to_image_size(drawable_id);
	}
	else{
		//TODO: this should raise an exception...
	}
}

GimpDrawable* index_image(gint32 image_id, guint transp_setting, gint dither_mode, gint indexer)
{
	///*Indexes an RGB or GRAY image to the MSX palette
	/**
	 * @param: image_id - 		gimp image id
	 * @param: transp_setting - Options for transparency - see enum TransparencyOptions
	 * @param: dither_mode - 	(dither modes of gimp-image-convert-indexed - 0 ... 3)
	 * @param: indexer - 		0 = gimp Internal, 1 = MSXize plugin by Weber Estevan Roder Kai
	 *
	 * @returns the indexed drawbale (or NULL if something went wrong)
	 */
	gint32 drawable_id;
	GimpDrawable *drawable;
	GimpParam * results;
	gboolean msxize_exists;
	gint nreturn_vals;

	// if MSXize is selected the image is already prepared and can be indexed with dither mode 0
	if (indexer == 1) {
		msxize_exists = gimp_procedural_db_proc_exists("plug-in-msxize");
		if (msxize_exists) {
			printf("Launching MSXize...\n");
			results = gimp_run_procedure("plug-in-msxize", &nreturn_vals,
					GIMP_PDB_INT32, GIMP_RUN_NONINTERACTIVE,
					GIMP_PDB_IMAGE, image_id,
					GIMP_PDB_DRAWABLE, gimp_image_get_active_drawable(image_id),
					GIMP_PDB_END);
			printf("MSXize done.\n");
			gimp_destroy_params(results, sizeof(results));
			dither_mode = 0;	// no dithering
		}
		else{
			g_message("Could not find MSXize plug-in installed!\n"
					"Please install the plug-in or select the internal indexer.\n");
			return NULL;
		}
	}

	if (transp_setting == USE_COLOR_FF00EB){
	  printf("Indexing image to MSX palette (incl. transparency)...\n");
	  gimp_image_convert_indexed(image_id, dither_mode, 4, NULL, FALSE, FALSE, "MSX Graphics mode II");
	}
	else{
	  printf("Indexing image to MSX palette (w.o. transparency)...\n");
	  gimp_image_convert_indexed(image_id, dither_mode, 4, NULL, FALSE, FALSE, "MSX Graphics mode II w.o. transparent");
	}

    // necessary to reassign the drawable after indexing!
    drawable_id = gimp_image_get_active_drawable(image_id);
    drawable = gimp_drawable_get(drawable_id);

    return drawable;
}

guchar* create_bload_header(guint16 size, guint16 address){
	/// Creates the header for using in MSX basic via the BLOAD command
	/**
	 * @param: size = size of binary data
	 * @param: address = target address of data in VRAM
	 */

	// FE, start address in bin file (16bit word), end address in bin file (16bit word),
	// execution address in RAM, VRAM (16 bit word) - all words little endian
	static guchar header[7]; //= {0xFE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

	header[0] = 0xFE;
	// size / end address
	header[3] = size & 0x00FF;
	header[4] = size >> 8;
	header[5] = address & 0x00FF;
	header[6] = address >> 8;

	return header;
}

