#!/usr/bin/env python
"""
msx_sprite_export.py - MSX sprite exporter plug-in for GIMP
Copyright (C) 2019, 2020 Thies Hecker

This file is part of the gimp-msx-plugins.

gimp-msx-plugins is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from __future__ import print_function
from gimpfu import *
import os
import sys


class Sprite:

    def __init__(self, xloc, yloc, size, color, sprite_data):
        """Sprite definition class

        Args:
            xloc(int): Origin of sprite in x-coordinates
            yloc(int): Origin of sprite in y-coordinates
            size(int): Sprite size (should either be 8 or 16)
            color(int): Color index (acc. to palette)
            sprite_data(list): Nested list (list or rows, each row contains the values (0 or 1) of \
                the pixels in the row)

        Attributes:
            xloc(int): Origin of sprite in x-coordinates
            yloc(int): Origin of sprite in y-coordinates
            size(int): Sprite size (should either be 8 or 16)
            color(int): Color index (acc. to palette)
            sprite_data(list): Nested list (list or rows, each row contains the values (0 or 1) of \
                the pixels in the row)

        """

        self.xloc = xloc
        self.yloc = yloc
        self.size = size
        self.color = color
        self.sprite_data = sprite_data

    @property
    def SPT(self):
        """Creates the sprite pattern table

        Returns:
            list: Byte values (int) representation of SPT
        """

        # convert sprite_data (if necessary)
        print("Sprite size = ", self.size)
        if self.size == 8:
            spt_data = self.sprite_data
        elif self.size == 16:
            spt_data_ul = [x[:8] for x in self.sprite_data[:8]]
            spt_data_bl = [x[:8] for x in self.sprite_data[8:]]
            spt_data_ur = [x[8:] for x in self.sprite_data[:8]]
            spt_data_br = [x[8:] for x in self.sprite_data[8:]]
            spt_data = spt_data_ul + spt_data_bl + spt_data_ur + spt_data_br
        else:
            raise ValueError(
                "Unsupported sprite size {} - must be either 8 or 16".format(self.size))

        print("SPT data:")

        # convert bits to byte value
        spt_bytes = []
        for i, bit_seq in enumerate(spt_data):
            spt_byte = 0
            for j, bit in enumerate(bit_seq):
                spt_byte += 2 ** (7 - j) * bit
            spt_bytes.append(spt_byte)
            print("byte {}: ".format(i), bit_seq, "; byte_val = {}".format(spt_byte))

        # convert list of bytes to binary
        return spt_bytes

    def create_SAT(self, sprite_no=0, early_clock_bit=False):
        """Creates the SAT entry for the sprite

        Args:
            sprite_no(int): Sprite number to be used in the name pointer byte (should be in \
                range 0...255)
            early_clock_bit(bool): Controls horizontal base position

        Returns:
            list: List of byte values for SAT entry (length is 4)

        Notes:

            * the sprite_no is to be understood always as the sprite count index - i.e. if 16x16 px
              sprites are used the method will automatically adjust the name pointer to the
              correct memory location (i.e. sprite no. 10 will point at byte 40 of the pattern
              table)
        """
        if self.size == 8:
            name_pointer = sprite_no
        elif self.size == 16:
            name_pointer = 4 * sprite_no
        else:
            raise ValueError("Invalid sprite size {} - must be either 8 or 16".format(self.size))

        if early_clock_bit:
            color_byte = self.color + 128
        else:
            color_byte = self.color

        sat_bytes = [
            self.yloc,
            self.xloc,
            name_pointer,
            int(color_byte)
        ]

        return sat_bytes


class SpriteCollection:

    def __init__(self, sprites):
        """Collection of sprite objects"""

        self.sprites = sprites

    @property
    def SAT(self):
        """bytearray: sprite attribute table for all sprites in collection"""

        sat_data = []  # should always be 4 values per sprite: vert. pos, horiz. pos.,
        # pointer number to SPT, color
        for i, sprite in enumerate(self.sprites):
            sat_data += sprite.create_SAT(sprite_no=i, early_clock_bit=False)

        return bytearray(sat_data)

    @property
    def SPT(self):
        """bytearray: sprite pattern table for all sprites in collection"""

        all_spt_bytes = []

        for sprite in self.sprites:
            all_spt_bytes += sprite.SPT

        return bytearray(all_spt_bytes)

    def write_SAT(self, filename):
        """Writes SAT to file"""

        with open(filename, "wb") as sat_file:
            sat_file.write(self.SAT)

    def write_SPT(self, filename):
        """Wrties SPT to file"""

        with open(filename, "wb") as spt_file:
            spt_file.write(self.SPT)


class SpriteRegion:

    def __init__(self, drawable, xloc, yloc, size, use_transp=False):
        """Data container for 8x8 or 16x16 image region, in which one or multiple sprites could be
        located

        Args:
            drawable(gimp.drawable): Drawable object
            xloc(int): Origin of sprite in x-coordinates
            yloc(int): Origin of sprite in y-coordinates
            size(int): Sprite size (should either be 8 or 16)
            use_transp(bool): If True, use alpha channel info to find transparent pixels
                              If False use indexed color 0 for transparent pixels

        Attributes:
            drawable(gimp.drawable): Drawable object
            xloc(int): Origin of sprite in x-coordinates
            yloc(int): Origin of sprite in y-coordinates
            size(int): Sprite size (should either be 8 or 16)
            use_transp(bool): If True, use alpha channel info to find transparent pixels
                              If False use indexed color 0 for transparent pixels
        """

        self.drawable = drawable
        self.xloc = xloc
        self.yloc = yloc
        self.size = size
        self.use_transp = use_transp

        # self.px_data = px_data
        # self.colors = colors

    @property
    def px_data(self):
        """list: A nested list (one list for each row - list elements are indexed color numbers of
        each pixel)"""

        px_regn = self.drawable.get_pixel_rgn(self.xloc, self.yloc, self.size, self.size)
        px_data_ = []
        for row in range(self.size):
            px_row = []
            for col in range(self.size):
                if self.use_transp:  # in this case we have an image with alpha channel
                    # element zero is index and element 1 is alpha value
                    px_alpha = ord(px_regn[self.xloc + col, self.yloc + row][1])
                    if px_alpha == 0:  # transparent
                        px_color = 0
                    else:
                        # add 1 to indexed color value since 15 color palette starts
                        # with MSX color 1 not 0 (i.e. transparent)
                        px_color = ord(px_regn[self.xloc + col, self.yloc + row][0]) + 1
                else:
                    if self.drawable.has_alpha:
                        px_color = ord(px_regn[self.xloc + col, self.yloc + row][0])
                    else:  # if no alpha channel only one value (index color) in px_regn
                        px_color = ord(px_regn[self.xloc + col, self.yloc + row])
                px_row.append(px_color)
            px_data_.append(px_row)

        return px_data_

    @property
    def colors(self):
        """list: Colors used in region"""
        color_data = []
        for row in self.px_data:
            color_data += row
        return list(set(color_data))


def get_sprites_from_region(sprite_region):
    """Extracts the individual sprites of a region

    Args:
        sprite_region(SpriteRegion):

    Returns:
        list: List of Sprite objects
    """

    # get sprite colors (w.o. color 0)
    sprite_colors = [color for color in sprite_region.colors if color != 0]

    sprites = []

    for color in sprite_colors:
        # fill sprite data with zeros
        sprite_data = []
        for i, row in enumerate(sprite_region.px_data):
            sprite_row_data = []
            for j, px in enumerate(row):
                if px == color:
                    sprite_row_data.append(1)
                else:
                    sprite_row_data.append(0)
            sprite_data.append(sprite_row_data)
        sprites.append(
            Sprite(sprite_region.xloc, sprite_region.yloc, sprite_region.size, color, sprite_data))

    return sprites


def find_sprites(drawable, sprite_size, use_transp):
    """Identifies sprites in the image/drawable

    Args:
        drawable(gimp.drawable): Drawable (layer) to analyze
        sprite_size(int): Sprite size either 8 or 16
        use_transp(bool): If True alpha channel will be used for transparency,
                          if False index color 0

    Returns:
        list: List of Sprite objects
    """
    sprite_size = int(sprite_size)

    sprite_locsx = int(drawable.width / sprite_size)
    sprite_locsy = int(drawable.height / sprite_size)

    # 1st get data for sprite region

    sprite_regions = []

    for locx in range(sprite_locsx):
        for locy in range(sprite_locsy):
            x_coord = int(locx * sprite_size)
            y_coord = int(locy * sprite_size)
            # print("x={}, y={}".format(x_coord, y_coord))

            sprite_regions.append(
                SpriteRegion(drawable, x_coord, y_coord, sprite_size, use_transp=use_transp))

    print("Found {} sprite regions.".format(len(sprite_regions)))
    for i, region in enumerate(sprite_regions):
        print("Region {}:".format(i))
        print("Coordinates: x = {}, y = {}".format(region.xloc, region.yloc))
        print("Colors: ", region.colors)
        print("Data of region:")
        for row in region.px_data:
            print(row)

    # now extract individual sprites of each region

    sprites = []
    for i, region in enumerate(sprite_regions):
        sprites += get_sprites_from_region(region)

    results_msg = "Found {} sprites in {} sprite regions".format(len(sprites), len(sprite_regions))
    print(results_msg)

    for i, sprite in enumerate(sprites):
        print("Sprite {}:".format(i))
        print("Coordinates: x = {}, y = {}".format(sprite.xloc, sprite.yloc))
        print("Color: ", sprite.color)
        print("Sprite data:")
        for row in sprite.sprite_data:
            print(row)
        print("SPT bytes:", sprite.SPT)
    return sprites


def index_image(drawable, use_transp):
    """Converts to indexed image

    Args:
        drawable(gimp.image): Drawable object
        use_transp(bool): Use 16 color palette with color for transparent if True - else 15 color \
            palette

    Returns:
        None
    """
    if drawable.is_indexed:
        print("Image already indexed - reverting to RGB for re-indexing...")
        # we will convert to RGB and re-index in order to avoid issues, when switching between
        # use_transp options...
        pdb.gimp_image_convert_rgb(drawable.image)

    if not use_transp:
        palette = "MSX Graphics mode II"
    else:
        palette = "MSX Graphics mode II w.o. transparent"
    # args are image, dither_type, palette_type, num_cols, alpha_dither, remove_unused, palette
    pdb.gimp_image_convert_indexed(drawable.image, 0, 4, 0, FALSE, FALSE, palette)


def check_image(drawable, sprite_size, use_transp):
    """Checks the drawable for suitability

    Args:
        drawable(gimp.drawable): drawable object
        sprite_size(int): Size of sprites (should be 8 or 16)
        use_transp(bool): True if alpha channel shall be used to indicate transparent pixels

    Returns:
        tuple: consisting of:

            * bool: True if checks okay
            * list: Error messages
    """

    valid = True
    errors = []

    sprite_size = int(sprite_size)

    if drawable.width % sprite_size != 0. or drawable.height % sprite_size != 0.:
        message = "Drawable dimensions are {} x {} pixels, but need to be multiples" \
                  " of sprite size (i.e. {} pixels)!\n" \
                  "Please adjust your image (or sprite) size accordingly.\n".format(drawable.width,
                                                                                    drawable.height,
                                                                                    sprite_size)
        errors.append(message)
        print(message)
        valid = False

    if sprite_size != 8 and sprite_size != 16:
        message = "Sprite size must be 8 or 16 pixels!\n"
        errors.append(message)
        print(message)
        valid = False

    if not drawable.has_alpha and use_transp:
        message = "The \"Use alpha channel for transparent pixels\" option has been selected, " \
                  "but the image does not have an alpha channel.\n" \
                  "Please add an alpha channel or deselect this option."
        errors.append(message)
        print(message)
        valid = False

    else:
        # check if any transparent (alpha=0) pixels exist (or if the user most likely selected
        # the wrong option)
        alpha_pixels_found = check_for_transparent_pixels(drawable=drawable)

        # return a warning messages for different cases
        if not alpha_pixels_found and use_transp:
            message = "The \"use alpha channel for transparent pixels\" option is selected, " \
                      "but no transparent pixels were found.\n" \
                      "You should export a ROM image to check the export results and eventually " \
                      "adjust the plugin options.\n" \
                      "If you don't want to use the alpha channel for transparent pixels " \
                      "de-select the option and paint all transparent pixels with color #ff00eb."
            errors.append(message)
            print(message)
        elif alpha_pixels_found and not use_transp:
            message = "The \"use alpha channel for transparent pixels\" option is not selected, " \
                      "but transparent pixels were found.\n\n" \
                      "Please either remove all transparent pixels (paint all transparent pixels " \
                      "with color #ff00eb) or select the \"use alpha channel for transparent " \
                      "pixels\" option."
            valid = False
            errors.append(message)
            print(message)

    return valid, errors


def check_for_transparent_pixels(drawable):
    """Checks if completely transparent pixels (alpha value = 0) are in the image

    Args:
        drawable(gimp.drawable): drawable object

    Returns:
        bool: True if any pixel with alpha value = 0 is in the image
    """

    # first check if the image has an alpha channel
    if drawable.has_alpha:
        px_regn = drawable.get_pixel_rgn(0, 0, drawable.width, drawable.height)
        alpha_pixels_found = False
        for row in range(drawable.height):
            for col in range(drawable.width):
                px_alpha = ord(
                    px_regn[col, row][1])  # element zero is index and element 1 is alpha value
                if px_alpha == 0:  # transparent
                    alpha_pixels_found = True
                    break
            if alpha_pixels_found:
                break
    else:
        alpha_pixels_found = False

    return alpha_pixels_found


def check_sprite_validity(sprite_collection):
    """Checks the sprites for validity (i.e. max. 4 sprites per scan line)

    Args:
        sprite_collection(SpriteCollection): Sprite collection object

    Returns:
        tuple: consisting of:

            * bool: True if checks okay
            * list: Error messages
    """
    # TODO: add check for max. number of sprites in VRAM (256 or 64)

    # check if more than 4 images are in one scan line
    warnings = []
    valid = True

    sprites_per_ycoord = {}
    for sprite in sprite_collection.sprites:
        if sprite.yloc not in sprites_per_ycoord.keys():
            sprites_per_ycoord[sprite.yloc] = 1
        else:
            sprites_per_ycoord[sprite.yloc] += 1

    for ycoord in sprites_per_ycoord.keys():
        no_sprites = sprites_per_ycoord[ycoord]
        if no_sprites > 4:
            valid = False
            warning_msg = "Warning: {} sprites found at y = {}!\n" \
                          "Officially the MSX (1) VDP only supports 4 sprites per scan line.\n" \
                          "Your sprite will not render correctly " \
                          "in the ROM image.".format(no_sprites, ycoord)
            print(warning_msg)
            warnings.append(warning_msg)

    return valid, warnings


def export_files(sprite_collection, image, split_files):
    """Exports SAT and SPT to file

    Args:
        sprite_collection(SpriteCollection): A sprite collection object
        image(gimp.image): A gimp image object
        split_files(bool): If True export each sprite SAT and SPT into a single file

    Returns:
        None

    Notes:
        * file names will be inherited from the image name and appended with the suffixes ".sat"
          for the sprite attribute tabel information and ".spt" for the sprite pattern table info
        * using the split_files option will append a "_<sprite_number>" before the extension
    """

    if split_files:
        dir_name = os.path.dirname(image.filename)
        no_sprites = len(sprite_collection.sprites)
        for i, sprite in enumerate(sprite_collection.sprites):
            sprite_no_str = str(i).zfill(
                int(math.ceil(math.log10(no_sprites))))  # add required number of padding zeros
            sat_filename = image.filename + "_{}.sat".format(sprite_no_str)
            spt_filename = image.filename + "_{}.spt".format(sprite_no_str)
            with open(sat_filename, "wb") as sat_file:
                sat_file.write(bytearray(sprite.create_SAT(sprite_no=i, early_clock_bit=False)))
            with open(spt_filename, "wb") as spt_filename:
                spt_filename.write(bytearray(sprite.SPT))
        message_text = "Wrote {} SAT and SPT files to folder:\n" \
                       "{}".format(no_sprites, dir_name)

    else:
        sat_filename = image.filename + ".sat"
        spt_filename = image.filename + ".spt"
        sprite_collection.write_SAT(sat_filename)
        sprite_collection.write_SPT(spt_filename)

        message_text = "SAT written to:\n{}\n" \
                       "SPT written to:\n{}".format(sat_filename, spt_filename)

    print(message_text)
    # pdb.gimp_message_set_handler(0)     # set message text to appear as message box
    # pdb.gimp_message(message_text)
    return message_text


def export_rom_image(sprite_collection, image, bg_color):
    """Exports to a ROM image

    Args:
        sprite_collection(SpriteCollection): Collection of sprites to export
        image(gimp.image): Image
        bg_color(int): MSX back ground color number (0...15)

    Returns:
        str: Message where file is saved
    """
    sprite_size = sprite_collection.sprites[0].size

    # default values for the 8 VDP registers
    vdp_values = [0x02, 0xE2, 0x0E, 0xFF, 0x03, 0x76, 0x03, 0x07]

    # adjust background color (VDP register 7)
    vdp_values[7] = int(bg_color)

    # if small sprites are used VDP reg 1, bit 6 needs to be set to 0
    if sprite_size == 8:
        vdp_values[1] = vdp_values[1] - 2

    rom_filename = image.filename + ".rom"

    # ROM header and execution code (see rom_template_sprites.asm for details)
    rom_header = [
        0x41, 0x42, 0x10, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x21, 0x00,
        0x58, 0x46, 0x0E, 0x00, 0xCD, 0x47, 0x00, 0x21, 0x01, 0x58, 0x46, 0x0E, 0x01, 0xCD, 0x47,
        0x00, 0x21, 0x02,
        0x58, 0x46, 0x0E, 0x02, 0xCD, 0x47, 0x00, 0x21, 0x03, 0x58, 0x46, 0x0E, 0x03, 0xCD, 0x47,
        0x00, 0x21, 0x04,
        0x58, 0x46, 0x0E, 0x04, 0xCD, 0x47, 0x00, 0x21, 0x05, 0x58, 0x46, 0x0E, 0x05, 0xCD, 0x47,
        0x00, 0x21, 0x06,
        0x58, 0x46, 0x0E, 0x06, 0xCD, 0x47, 0x00, 0x21, 0x07, 0x58, 0x46, 0x0E, 0x07, 0xCD, 0x47,
        0x00, 0x21, 0x00,
        0x00, 0x01, 0x00, 0x40, 0x3E, 0x00, 0xCD, 0x56, 0x00, 0x21, 0x00, 0x48, 0x01, 0x00, 0x08,
        0x11, 0x00, 0x18,
        0xCD, 0x5C, 0x00, 0x21, 0x00, 0x50, 0x01, 0x80, 0x00, 0x11, 0x00, 0x3B, 0xCD, 0x5C, 0x00,
        0x00, 0x18, 0xFD
    ]
    with open(rom_filename, "wb") as rom_file:
        rom_file.write(bytearray([0x00] * 8192))  # fill 8k ROM with zeros

        # write header at pos. 0
        rom_file.seek(0)
        rom_file.write(bytearray(rom_header))

        # write SPT
        rom_file.seek(0x0800)  # address for SPT (see rom_template_sprites.asm for details)
        rom_file.write(sprite_collection.SPT)

        # write SAT
        rom_file.seek(0x1000)  # address for SPT (see rom_template_sprites.asm for details)
        rom_file.write(sprite_collection.SAT)

        # write VDP default values at 1800h (see rom_template_sprites.asm for details)
        rom_file.seek(0x1800)
        rom_file.write(bytearray(vdp_values))

    message_text = "ROM image exported:\n{}".format(rom_filename)
    return message_text


def redirect_output(log_file):
    """Redirects stdout and stderr to files
    
    Args:
        log_file(str): File name where to log the output of the print commands
    
    This is required to avoid crashes on MS win 
    (when python is launched without console)
    """

    sys.stdout = sys.stderr = open(log_file, "w")


def msx_sprite_export_main(cur_img, cur_drawable, sprite_size, use_transp, check_only, split_files,
                           export_rom,
                           bg_color):
    """This is the main function called by the plugin

    Args:
        cur_img(gimp.image): Image object
        cur_drawable(gimp.drawable): Drawable object
        sprite_size(int): Sprite size (either 8 or 16)
        use_transp(bool):
        check_only(bool): If True perform checks only - do not export data
        split_files(bool): If True export each sprite into separate SAT and SPT files
        export_rom(bool): If True export a ROM image displaying the sprite (useful for testing)
        bg_color(int): Background color used for the ROM image (should be 0...16)

    Returns:
        None
    """
    home = os.path.expanduser("~")
    log_file = "gimp_msx_sprite_exporter.log"
    log_path = os.path.join(home, log_file)

    messages = []

    redirect_output(log_path)  # call this first - otherwise this plugin might crash on MS win...
    messages.append("Log file written to {}".format(log_path))

    pdb.gimp_message_set_handler(0)  # set message text to appear as message box

    # index_image must be called before find_sprites!
    index_image(cur_drawable, use_transp=use_transp)

    valid_image, errors = check_image(drawable=cur_drawable, sprite_size=sprite_size,
                                      use_transp=use_transp)

    error_str = ""
    for error_msg in errors:
        error_str += error_msg + "\n"
    # pdb.gimp_message(error_str)
    messages.append(error_str)

    if valid_image:
        sprite_collection = SpriteCollection(
            find_sprites(cur_drawable, sprite_size=sprite_size, use_transp=use_transp))

        results_msg = "Found {} sprites in image".format(len(sprite_collection.sprites))
        messages.append(results_msg)

        valid_sprites, warnings = check_sprite_validity(sprite_collection=sprite_collection)
        if not valid_sprites:
            warn_str = ""
            for warning in warnings:
                warn_str += warning + "\n"
            # pdb.gimp_message(warn_str)
            messages.append(warn_str)

        if not check_only:
            if cur_img.filename is None:
                messages.append("You need to save the image first, before exporting sprites!\n"
                                "Please save your image and rerun the plugin.")
            else:
                messages.append(export_files(sprite_collection, cur_img, split_files=split_files))

                if export_rom:
                    messages.append(export_rom_image(sprite_collection, cur_img, bg_color))

    messages_str = ""
    for message in messages:
        messages_str += message + "\n\n"

    pdb.gimp_message(messages_str)

    # convert the image back to RGB
    # TODO: this assumes an RGB image is the source always - should be improved to convert back to
    #  original color model
    pdb.gimp_image_convert_rgb(cur_img)


# register does not seem to work correctly with keyword arguments (except for optional like menu)...
register(
    "python-fu-msx_sprite_ex",
    "MSX sprite exporter",
    "Export sprites to MSX(1) VDP (TI TMS9918) compatible binary format",
    "Thies Hecker",
    "Thies Hecker",
    "2019",
    "MSX sprite exporter",
    "RGB*, GRAY*, INDEXED*",
    [
        (PF_IMAGE, "cur_img", "Current image", None),  # type, arg_name, GUI label, default value
        (PF_DRAWABLE, "cur_drawable", "Current drawable", None),
        (PF_SPINNER, "sprite_size", "Sprite size", 16, [8, 16, 8]),
        # additional parameter for PF_SPINNER [min, max, step]
        (PF_TOGGLE, "use_transp", "Use alpha channel for transparent pixels", TRUE),
        (PF_TOGGLE, "check_only", "Check only", FALSE),
        (PF_TOGGLE, "split_files", "Separate files for each sprite", FALSE),
        (PF_TOGGLE, "export_rom", "Export ROM image", TRUE),
        (PF_SPINNER, "bg_color", "Background color (for ROM image)", 1, [0, 16, 1])
    ],
    [],
    msx_sprite_export_main,
    menu="<Image>/Filters/Misc")

main()
