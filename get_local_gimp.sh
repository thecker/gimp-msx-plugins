#!/bin/sh
#tries to extracts path to local GIMP folder
gimpversionstr=`gimp --version`
GIMP_VERSION=`echo $gimpversionstr | $GREPCMD -o '[0-9]\.[0-9]+'`
FILE=$HOME/.gimp-$GIMP_VERSION
FILE2=$HOME/.config/GIMP/$GIMP_VERSION

if [ -d "$FILE" ]; then
    echo "$FILE"
else
	if [ -d "$FILE2" ]; then
		echo "$FILE2"
	fi
fi
